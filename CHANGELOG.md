# Changelog

## v0.1.1 - 10-12-2025

### Fixed

- Multiple big fixes

### Added

- Fist tag

### Changed

- Multiple changes

### Deprecated

_Nothing_

### Removed

_Nothing_

