'''
General utils for clean-data.
'''
from scipy import stats

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def combine_nested_dicts(dict1, dict2):
    '''
    Combines the content of two dictionaries and assigns the values of potential
    duplicate keys to a sinlge dictionary key.
    
    Parameters
    ----------
    dict1, dict2 : dict [`any`, `dict`]
        Dictionaries with potential overlapping keys and dicts as values.
    Returns
    -------
    combined : dict
        A dictionary combining the keys and values of the input dicts.
    '''
    combined = dict1.copy()  # Start with a copy of the first dictionary
    for key, inner_dict in dict2.items():
        if key in combined:
            # If the key exists in both dictionaries, merge the inner dictionaries
            for sub_key, value in inner_dict.items():
                combined[key][sub_key] = value
        else:
            # If the key doesn't exist, add it as is
            combined[key] = inner_dict
    return combined
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def likelihood_ratio_test(ll_full:float, ll_sub:float, df:int,
                          ) -> tuple[float, float]:
    '''
    Calculate the likelihood ration test comparing a subset model which is
    nested in the full model.
    
    Parameters
    ----------
    ll_full : `float`
        The log-likelihood of the full model.
    ll_sub : `float`
        The log-likelihood of the sub model.
    df : `int`
        The difference in coefficients between both models, should be positive.
    
    Returns
    -------
    results : `tuple` [`float`, `float`]
        The Likelihood ratio test statistics,
        The p-value.
    '''
    # Calculate the test statistic
    lr_stat = 2 * (ll_full - ll_sub)
    pvalue = stats.chi2.sf(lr_stat, df).item()
    # return
    return lr_stat, pvalue
