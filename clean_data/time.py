#!/usr/bin/env python3
'''
Functions to curate time to event data.
'''

from typing import Any, List, Type, Union, Tuple, Optional, Dict
from clean_data.errors import (
    is_type,
    is_df,
    are_columns_in_df,
)
from clean_data.constants import(
    PruneNames,
)
import pandas as pd
import sys
import numpy as np
import warnings

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class FollowUpResults(object):
    '''
    The `create_follow_up` results objects
    
    Attributes
    ----------
    data : pd.DataFrame
        DataFrame with `follow_up` and/or `event_time` columns appended.
    rows_with_non_positive_follow_up : list of string,
        records the indices dropped because of a follow-up time equal or
        smaller than zero.
    rows_with_non_positive_event_time : list of string,
        records the indices dropped because of an event time equal or smaller
        than zero.
    
    Note that the two removed_rows_* objects will typically differ due to
    the presence of missing data in the various event dates.
    '''
    SET_ARGS = [
        PruneNames.DATA,
        PruneNames.INDEX_FOLLOW,
        PruneNames.INDEX_EVENT_TIME,
    ]
    # Initiation the class
    def __init__(self, **kwargs):
        """
        Initialise
        """
        for k in kwargs.keys():
            if k not in self.__class__.SET_ARGS:
                raise AttributeError("unrecognised argument '{0}'".format(k))
        # Loops over `SET_ARGS`, assigns the kwargs content to name `s`.
        for s in self.__class__.SET_ARGS:
            try:
                setattr(self, s, kwargs[s])
            except KeyError:
                warnings.warn("argument '{0}' is set to 'None'".format(s))
                setattr(self, s, None)
    # /////////////////////////////////////////////////////////////////////////
    def __str__(self):
        return f"A `FollowUpResults` class."

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_follow_up(data:pd.DataFrame, date_dict:Dict[str, str],
                     follow_up:Union[Dict[str, List[str]], None]=None,
                     follow_up_drop_zero:bool=True,
                     event_time:Union[Dict[str,List[str]], None]=None,
                     event_time_drop_zero:Union[List[bool],bool]=False,
                     time_unit:str='days',
                     **kwargs:Optional[Any],
                     ) -> FollowUpResults:
    '''
    A function to map dates to `pd.to_datetime` and create optional follow-up,
    and time to event columns. Note `follow-up` is simply the time since start
    of follow-up until the last study visit, and `time to event` the time to
    a specific event has occurred _or_ last study visit (censoring).
    
    Parameters
    ----------
    data : pd.DataFrame
    date_dict : dict
        The dict keys reflect the new date columns, the dict values the crude
        date column names. Both should be supplied as strings.
    follow_up : dict, default `NoneType`
        A dictionary of length 1, with key reflecting the new follow-up column
        name, and a list of strings as value reflecting the (1) the start of
        follow-up (as a date) and (2) the end of follow-up (as a date).
        Will be ignored when follow-up is `None`.
    follow_up_drop_zero : bool, default True
        If rows with a follow-up time of zero or smaller should be dropped
        before considering any more calculation of event time. Will also remove
        rows with missing dates and follow-ups.
    event_time : dict of strings, default `NoneType`
        A dictionary with 1 or more entries. The keys reflect the new event
        time columns, the values should be a list with two entries reflecting
        (1) the start of follow-up (as date) and (2) the date an event has
        occurred (as date). Will either take the minimum of event date and
        follow-up date (if follow_up is not `None`) or otherwise simply the
        difference between start of follow-up and the event date.
    event_time_drop_zero : bool or list of bools, default False
        If rows with negative event times should be dropped. Will also remove
        rows with a missing event date. set to False if you rather censor these
        observations.
    time_unit : str, default 'days'
        The pd.datatime unit.
    **kwargs: any
        keyword arguments for `pd.to_datetime`.
    
    Returns
    -------
    FollowUpResults : Class
        A class with the curated table, as well as the lists with
        row indices which were removed due to incorrect follow-up or dates.
    '''
    # ### internal function
    def _create_time_diff(data, column, diff_list, time_unit, drop):
        '''
        Calculates the follow-up time and optionally removes follow-up equal or
        smaller to zero.
        
        Returns
        -------
        pd.DataFrame, List(str):
            Will return a column with `column` added, and a (possibly empty)
            list with indices removed due to `drop`.
        '''
        data[column] = data[diff_list[1]] - data[diff_list[0]]
        # setting to float
        data[column] = data[column].dt.__getattribute__(time_unit)
        # drop negative
        if drop == True:
            keep = data[column] > 0
            data = data[keep].copy()
            # get list of removed indices
            drop = keep.index[keep == False]
            drop = drop.to_list()
        else:
            drop = []
        # return
        return data, drop
    # ### check input
    is_df(data)
    # copy
    data = data.copy()
    # is_type(data, pd.DataFrame)
    is_type(follow_up, (dict, type(None)))
    is_type(event_time, (dict, type(None)))
    is_type(follow_up_drop_zero, bool)
    is_type(event_time_drop_zero, (List, bool))
    # making sure event_Time_drop_zero is a list
    if (event_time is not None) and isinstance(event_time_drop_zero, bool):
        event_time_drop_zero = [event_time_drop_zero] * len(event_time)
    elif (event_time is not None) and isinstance(event_time_drop_zero, List):
        if len(event_time) != len(event_time_drop_zero):
            raise ValueError('`event_time_drop_zero` should either be a single '
                             'boolean value, of a list of bools equal to the '
                             'number of entries in `event_time`.'
                             )
    if follow_up is not None:
        if len(follow_up) != 1:
            raise ValueError('Follow-up time should consist of a single key '
                             'with a list of strings referring to the start '
                             'and end data columns.')
    # ### the actual work
    # mapping dates
    for target, source in date_dict.items():
        data[target] = pd.to_datetime(data[source],
                                      **kwargs)
    # creating follow-up time
    removed_rows_follow_up_time = []
    if follow_up is not None:
        follow_col=[k for k in follow_up.keys()][0]
        follow_dif=[i for i in follow_up.values()][0]
        data, dropped_indices = _create_time_diff(
            data, column=follow_col, diff_list=follow_dif, time_unit=time_unit,
            drop=follow_up_drop_zero,
        )
        # add rows that were removed
        removed_rows_follow_up_time = removed_rows_follow_up_time +\
            dropped_indices
    # create time to event columns
    removed_rows_event_time = []
    if event_time is not None:
        for it, (column, list) in enumerate(event_time.items()):
            data, dropped_indices = _create_time_diff(data, column=column, diff_list=list,
                                     time_unit=time_unit,
                                     drop=event_time_drop_zero[it])
            # add rows that were removed
            removed_rows_event_time = removed_rows_event_time + dropped_indices
            if follow_up is not None:
                # NOTE taking the minimum only work for fatel events of course
                # instead just replace the NAs by follow-up
                data.loc[data[column].isnull(), column] =\
                    data.loc[data[column].isnull(), follow_col]
    # create dict
    results_dict = {PruneNames.DATA:data,
                    PruneNames.INDEX_FOLLOW: removed_rows_follow_up_time,
                    PruneNames.INDEX_EVENT_TIME: removed_rows_event_time,
                    }
    # return data
    return FollowUpResults(**results_dict)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def correct_time(data:pd.DataFrame, test_time_col:str, max_time_col:str,
                 correct_time_col:str,
                 dependent_time_columns:Union[List[str], None]=None,
                 verbose:bool=False,
                 ) -> pd.DataFrame:
    '''
    Function to correct time to event data. Often the supplied follow-up data
    is approximate, which can result into people having time to event data,
    _after_ the last recorded moment of follow-up. Typically one would
    remove these data. In some cases a logical reason might exist - although
    this should have been recorded better, which the current function
    attempts to address.
    
    The function is fairly straightforward, it identifies rows for which the
    `test_time` is larger than the `max_time`. For these `immortal_rows`,
    the `test_time` and `max_time_col` get truncated to the `correct_time`.
    Will also apply the `test_time` to an optional number of
    `dependent_time_columns`.
    
    Parameters
    ----------
    data : pd.DataFrame
    test_time_col : str
        The name of the column (as floats) you want to test.
    max_time_col : str
        The name of the column (as floats) that represent the maximum follow-up.
    correct_time_col : str
        The name of the column (as floats) that represent the correct time.
    dependent_time_columns : list of str, default `NoneType`
        An optional list of column names which also need to be assigned the
        `correct_time`.
    verbose : bool, default False
        Prints the number of affected rows.
    
    Returns
    -------
    data : pd.DataFrame
        The curated table.
    '''
    
    # ### test input
    is_df(data)
    is_type(test_time_col, str)
    is_type(max_time_col, str)
    is_type(correct_time_col, str)
    is_type(dependent_time_columns, (list, type(None)))
    is_type(verbose, bool)
    are_columns_in_df(data, [test_time_col, max_time_col, correct_time_col])
    # #### do work
    fix_list = [test_time_col] + dependent_time_columns
    for col in fix_list:
        immortal_col = data[col] > data[max_time_col]
        if verbose == True:
            print('{}: {} rows will be set to `correct_time`.'.\
                  format(str(col), str(sum(immortal_col))),
                  file = sys.stdout
                  )
        data.loc[immortal_col, col] = data.loc[immortal_col,
                                               correct_time_col]
    # done, return stuff
    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def truncated_event_indicator(data:pd.DataFrame, time_col:str,
                               time_threshold:float,
                               event_indicator_all_time:str,
                               event_target_value:Union[str,int,float],
                               event_col:str,
                               truncate_time_suffix:Union[None,str]=None,
                               ) -> pd.DataFrame:
    '''
    Creates a binary indicator (1 for an event) for any event that occured before
    `time_threshold`. Rows with less follow-up then `time_threshold` which
    did not have an event `event_indicator == event_target_value` are set to
    `np.nan'. Can optionally record the truncated time in a new column.
    
    Parameters
    ----------
    data : pd.DataFrame
    time_col : str
        The column name of the follow-up time (as a float).
    time_threshold : float
        The maximum follow-up to record accrued cases.
    event_indicator_all_time : str
        The column name of the indicator recarding the cases accrued over
        _all_ available time (without truncation).
    event_target_value : int
        The event indicator value.
    event_col : str
        The target column name that will be added to `data`
    truncate_time_suffix: str, default `NoneType`
        If supplied, a new column will be added recording the truncated
        follow-up time. The column name will be
        `time_col + truncate_time_suffix`. Set to `NoneType` to not add this
        column.
    
    Returns
    -------
    data : pd.DataFrame
        A table with an updated binary event indicator.
    '''
    # ### test input
    is_df(data)
    is_type(event_col, str)
    is_type(time_col, str)
    is_type(event_indicator_all_time, str)
    is_type(event_target_value, (float,int, str, List))
    is_type(time_threshold, (int, float))
    is_type(truncate_time_suffix, (str, type(None)))
    # ### do the work
    # make sure this is a list
    if isinstance(event_target_value, List) == False:
        event_target_value = [event_target_value]
    # empty column
    data[event_col] = 0
    # which rows have an event
    event_row = (data[time_col] < time_threshold) &\
        (data[event_indicator_all_time].isin(event_target_value))
    # which rows do not have sufficient follow-up time
    nan_row = (data[time_col] < time_threshold) &\
        (data[event_indicator_all_time].isin(event_target_value) == False)
    # create indicator
    data.loc[event_row, event_col] = 1
    data.loc[nan_row, event_col] = np.nan
    # ### should the time column be truncated
    if truncate_time_suffix is not None:
        new_time_col = time_col + str(truncate_time_suffix)
        # copy old time and truncate
        data[new_time_col] = data[time_col]
        data.loc[data[time_col] > time_threshold, new_time_col] =\
            time_threshold
    # return data
    return data

