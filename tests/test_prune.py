#!/usr/bin/env python3
from clean_data.constants import PruneNames as PNames
from clean_data.example_data import examples
from clean_data import prune
import numpy as np

'''
Tests for `clean_data.prune`
'''
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestPairwise_Correlation(object):
    '''
    Testing functions for the `pairwise_correlation` module
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_pairwise_correlation(self):
        # getting data
        data = examples.patient_characteristics(15, 6)
        # results
        res1, res2 = prune.pairwise_correlation(data, threshold=0.4)
        assert res1.shape == (5, 2)
        assert res2.shape == (8, 8)
        assert res2.sum().round(2).to_list() == \
            [0.72, 1.55, -0.43, 0.30, 0.63, 1.02, 0.13, 0.87]
        assert np.round(res1[PNames.CORRELATION].mean(), 2) == 0.58
