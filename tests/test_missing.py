"""
Tests for clean_data.missing.
"""
from clean_data import missing
from clean_data.example_data import examples
from clean_data.errors import (
    InputValidationError,
)
import pytest
import pandas as pd

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
data = examples.patient_characteristics(prop_nan=0.4, seed=25101986)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestMissingData(object):
    '''
    Testing functions for the `MissingData` class
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_expected(self):
        MD = missing.MissingData(data)
        assert isinstance(MD.data, pd.DataFrame)
        assert MD.data.shape == data.shape
        assert MD.missings_per_column.to_list() == [6, 4, 2]
        assert MD.missings_per_row.to_list()[0:2] == [2, 2]
        assert MD.missings_per_row_prop.to_list()[-3:] == [1/3, 1/3, 0.0]
        assert MD.filter(0.0, negate=True) == [0]
        assert set(MD.filter(0.2, negate=False)) == set([6, 3, 8, 1, 5, 2, 7, 4, 9])
        assert MD.filter(0.45, negate=True, axis=1) == ['x2', 'x3']
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_errors(self):
        with pytest.raises(InputValidationError):
            _ = missing.MissingData([])
        # getting an instance
        MD = missing.MissingData(data)
        with pytest.raises(ValueError):
            MD.filter(-1.0)
        with pytest.raises(ValueError):
            MD.filter(1.1)
        with pytest.raises(InputValidationError):
            MD.filter(1)
