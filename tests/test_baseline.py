from scipy import stats
from clean_data import baseline
from clean_data.example_data import examples
from clean_data.constants import BaselineNames as BNames
from clean_data.constants import BaselineTests as BTests
from statsmodels.regression.linear_model import RegressionResultsWrapper
import pytest
import numpy as np
import pandas as pd

'''
Tests for `clean_data.baseline`
'''
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# constants and fixtures
test_dict = BTests()
data_params = (15, 2, 3, 3, 2)
data_params2 = (15*2, 2, 3, 3, 2)
# Helper function to generate the repeating structure
def create_var_dict():
    return {
        BNames.REG_ESTIMATES: pd.DataFrame([0.2, 0.4], index=['x1', 'x2']),
        BNames.REG_SE: pd.DataFrame([0.1, 0.04], index=['x1', 'x2']),
        BNames.REG_PVALUE: pd.DataFrame([0.01, 0.004], index=['x1', 'x2']),
        BNames.REG_MODEL: None,
        BNames.REG_CASES: 400,
        BNames.REG_SAMPLES: 4000,
    }
@pytest.fixture
def reg_results():
    return {
        # reg_results = {
        'OLS1': {'var1': create_var_dict(), 'var2': create_var_dict()},
        'OLS2': {'var1': create_var_dict(), 'var2': create_var_dict()},
    }

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestBaselineTableStrata(object):
    '''
    Testing functions for the `BaselineTableStrata` class
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_baseline_table_innit(self):
        data = examples.patient_characteristics(*data_params)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4', 'x5'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        table_inst2 = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4', 'x5'],
            categorical_variables=['x10'],
            strata_name='x9',
            strata_sort=False,
        )
        # asserts
        assert getattr(table_inst, BNames.CLS_STRATA) == \
            ['string0', 'string1', 'string2', 'string3']
        assert getattr(table_inst2, BNames.CLS_STRATA) == \
            ['string3', 'string2', 'string0', 'string1']
        with pytest.raises(ValueError):
            _ = baseline.BaselineTableStrata(
                data=data,
                continuous_variables=['x1', 'x3'],
                binary_variables=['x4', 'x5'],
                categorical_variables=['x10'],
                strata_name='x9',
                strata_max=2,
            )
        assert isinstance(table_inst.__str__(), str)
        assert isinstance(table_inst.__repr__(), str)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_baseline_table_call(self):
        data = examples.patient_characteristics(*data_params, prop_nan=0.15)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4', 'x5'],
            categorical_variables=['x10'],
            strata_name='x9',
            verbose=False,
        )
        # get stratified baseline table
        tab1 = getattr(table_inst(), BNames.CLS_BASELINE)
        tab2 = getattr(table_inst(missings='pairwise'), BNames.CLS_BASELINE)
        tab3 = getattr(table_inst(round = 4), BNames.CLS_BASELINE)
        tab4 = getattr(table_inst(replace_na='.'), BNames.CLS_BASELINE)
        # assert
        assert tab1.loc['x3'].iloc[10] == 'nan (nan; nan)'
        assert tab2.loc['x3'].iloc[10] == '1.00 (1.00; 1.00)'
        assert tab3.loc['x1'].iloc[10] == '-0.6807 (-0.7851; -0.1595)'
        assert tab4.loc['x3'].iloc[10] == '.'
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_tests(self):
        data = examples.patient_characteristics(*data_params)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4', 'x5'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        pval = table_inst._strata_tests('x1',
                                        test_dict=test_dict.kruskal_wallis)
        ints_method = table_inst._strata_tests(
            'x1', test_dict=test_dict.kruskal_wallis, return_instance=True,
        )
        assert pval < 1.0
        assert isinstance(ints_method, stats._stats_py.KruskalResult)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_tests_bin(self):
        data = examples.patient_characteristics(*data_params2)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x5',
        )
        pval = table_inst._strata_tests(
            'x4', test_dict=test_dict.fisher_exact, table=True)
        # assert
        assert pval < 1.0
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_tests_cat(self):
        data = examples.patient_characteristics(*data_params)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        pval = table_inst._strata_tests(
            'x10', test_dict=test_dict.chi_square, table=True)
        # assert
        assert pval < 1.0
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_regression_ols(self):
        data = examples.patient_characteristics(*data_params)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        pval, res_dict = table_inst._strata_regression(
            'x1', test_dict=test_dict.ols_regression)
        assert pval < 1.0
        assert isinstance(res_dict[BNames.REG_ESTIMATES], pd.DataFrame)
        assert isinstance(res_dict[BNames.REG_MODEL],
                          RegressionResultsWrapper)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_regression_cat(self):
        data = examples.patient_characteristics(*data_params)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        pval, res_dict = table_inst._strata_regression(
            'x1', test_dict=test_dict.ols_regression, strata_ref='string1',)
        assert pval < 1.0
        assert isinstance(res_dict[BNames.REG_ESTIMATES].sum()[0], float)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_regression_noint(self):
        data = examples.patient_characteristics(*data_params)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        # adjust dict
        ols_dict = test_dict.ols_regression
        ols_dict[BNames.TEST_PVALUE] = 0
        pval, res_dict = table_inst._strata_regression(
            'x1', test_dict=ols_dict, intercept=False,)
        assert pval < 1.0
        assert isinstance(res_dict[BNames.REG_ESTIMATES].sum()[0], float)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_regression_covariate(self):
        data = examples.patient_characteristics(*data_params2)
        cov = examples.patient_characteristics(data_params2[0], 2, 0, 0)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        pval, res_dict = table_inst._strata_regression(
            'x1', test_dict=test_dict.ols_regression, cov=cov,)
        assert pval < 1.0
        assert res_dict[BNames.REG_ESTIMATES].shape == (4,1)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_regression_cov_indices(self):
        data = examples.patient_characteristics(*data_params2)
        cov = examples.patient_characteristics(data_params2[0], 2, 0, 0)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        # removing some rows
        cov2 = cov.drop(28)
        # also invert cov2
        cov2 = cov2.iloc[::-1]
        with pytest.raises(IndexError):
            _ = table_inst._strata_regression(
                'x1', test_dict=test_dict.ols_regression, cov=cov2,
                cov_confirm_indices = True)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_strata_regression_glm(self):
        data = examples.patient_characteristics(*data_params2)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )
        pval, _ = table_inst._strata_regression(
            'x4', test_dict=test_dict.logistic_regression)
        assert pval < 1.0
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_create_regression_table(self, reg_results):
        tab = baseline.BaselineTableStrata._create_regression_table(
            reg_results
        )
        assert tab.shape == (8, 7)
        assert tab.columns.isin([BNames.REG_VAR, BNames.REG_SE]).sum() == 2
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_create_regression_table_empty(self):
        tab = baseline.BaselineTableStrata._create_regression_table(
            {}
        )
        assert isinstance(tab, pd.DataFrame)
        assert tab.empty == True
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_del_rows(self):
        data = examples.patient_characteristics(*data_params2, prop_nan=0.20)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )()
        # the actual method
        data2 = table_inst._del_rows('x1')
        assert data2.shape == (20, 2)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_test_difference(self):
        data = examples.patient_characteristics(*data_params2)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )()
        # the actual method
        table = table_inst.test_difference(
            continuous_tests= {
                'Kruskal-Wallis': test_dict.kruskal_wallis,
                'Anova': test_dict.one_way_anova,
                'OLS': test_dict.ols_regression,
            },
            binary_tests = {
                'Chi-square': test_dict.chi_square
            },
            categorical_tests = {
                'Chi-square': test_dict.chi_square
            },
        )
        assert table.empty == False
        assert 'Difference' in table.columns
        assert getattr(table_inst, BNames.CLS_BASELINE_DIF).shape ==\
            table.shape
        assert getattr(table_inst, BNames.CLS_REGRESSION).shape == (4, 7)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_test_difference_strata_ref(self):
        data = examples.patient_characteristics(*data_params2)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )()
        # the actual method
        table = table_inst.test_difference(
            continuous_tests= {
                'Kruskal-Wallis': test_dict.kruskal_wallis,
                'Anova': test_dict.one_way_anova,
                'OLS': test_dict.ols_regression,
            },
            binary_tests = {
                'Chi-square': test_dict.chi_square,
                'Logistic Regression': test_dict.logistic_regression,
            },
            categorical_tests = {
                'Chi-square': test_dict.chi_square
            },
            strata_ref='string2', compact = True,
        )
        assert table.empty == False
        assert getattr(table_inst, BNames.CLS_BASELINE_DIF).shape ==\
            table.shape
        assert getattr(table_inst, BNames.CLS_REGRESSION).shape == (12, 7)
        assert not 'x9: string2' in\
            getattr(table_inst, BNames.CLS_REGRESSION)[BNames.REG_EXPOS].unique()
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_test_difference_coef_format(self):
        data = examples.patient_characteristics(*data_params2)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )()
        # the actual method
        table = table_inst.test_difference(
            continuous_tests= {
                'Kruskal-Wallis': test_dict.kruskal_wallis,
                'Anova': test_dict.one_way_anova,
                'OLS': test_dict.ols_regression,
            },
            binary_tests = {
                'Chi-square': test_dict.chi_square,
                'Logistic Regression': test_dict.logistic_regression,
            },
            categorical_tests = {
                'Chi-square': test_dict.chi_square
            },
            strata_ref='string0', compact = False,
            pval_format={BNames.CLS_PVAL_FORM_SCI: {}},
            estimate_format={},
        )
        assert table.empty == False
        assert getattr(table_inst, BNames.CLS_BASELINE_DIF).shape ==\
            table.shape
        assert BNames.REG_FORMAT_EST2 in\
            getattr(table_inst, BNames.CLS_REGRESSION).columns
        assert all(
            isinstance(s, str) for s in\
            getattr(table_inst, BNames.CLS_REGRESSION)[BNames.REG_FORMAT_EST])
        assert all(isinstance(s, str) for s in\
                   getattr(table_inst, BNames.CLS_BASELINE_DIF)[
                       ('Difference',      'Kruskal-Wallis')] if not pd.isna(s))
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_test_difference_pairwise(self):
        data = examples.patient_characteristics(*data_params2, prop_nan=0.3)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )()
        # the actual method
        table = table_inst.test_difference(
            missings='pairwise',
            continuous_tests= {
                'Kruskal-Wallis': test_dict.kruskal_wallis,
                'Anova': test_dict.one_way_anova,
            },
            binary_tests = {
                'Chi-square': test_dict.chi_square,
                            },
            categorical_tests = {
                'Chi-square': test_dict.chi_square
            },
        )
        assert table.empty == False
        assert getattr(table_inst, BNames.CLS_BASELINE_DIF).shape ==\
            table.shape
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_test_difference_listwise(self):
        data = examples.patient_characteristics(*data_params2, prop_nan=0.3)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x3'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x9',
        )()
        # the actual method
        table = table_inst.test_difference(
            missings='listwise',
            continuous_tests= {
                'Kruskal-Wallis': test_dict.kruskal_wallis,
                'Anova': test_dict.one_way_anova,
            },
            binary_tests = {
                'Chi-square': test_dict.chi_square,
                            },
            categorical_tests = {
                'Chi-square': test_dict.chi_square
            },
        )
        assert table.empty == False
        assert getattr(table_inst, BNames.CLS_BASELINE_DIF).shape ==\
            table.shape
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_test_difference_pairwise_reg(self):
        data = examples.patient_characteristics(*data_params2, prop_nan=0.3)
        table_inst = baseline.BaselineTableStrata(
            data=data,
            continuous_variables=['x1', 'x2'],
            binary_variables=['x4'],
            categorical_variables=['x10'],
            strata_name='x3',
        )()
        # the actual method
        table = table_inst.test_difference(
            missings='pairwise',
            continuous_tests= {
                'Kruskal-Wallis': test_dict.kruskal_wallis,
                'OLS': test_dict.ols_regression,
            },
        )
        assert table.empty == False
        assert getattr(table_inst, BNames.CLS_BASELINE_DIF).shape ==\
            table.shape
        assert getattr(table_inst, BNames.CLS_REGRESSION).shape == (4, 7)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TestBaseline(object):
    '''
    Testing functions for the `baseline` module
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_ColumnSummary(self):
        # getting data
        data = examples.patient_characteristics(15, 3, 3, 3)
        var0 = data.columns[0]
        var4 = data.columns[4]
        var8 = data.columns[8]
        # summary 1
        sum1 = baseline.ColumnSummary(data)
        res1 = sum1.symmetrical(var0, dropna=True)
        res2 = sum1.skewed(var0, dropna=True)
        assert np.round(res1[BNames.MEAN], 2) == 0.42
        assert np.round(res1[BNames.SD], 2) == 0.88
        assert res1[BNames.N] == 15
        assert res1[BNames.MISSING] == 0
        assert np.round(res2[BNames.MEDIAN], 2) == 0.60
        assert np.round(res2[BNames.Q1], 2) == -0.23
        assert np.round(res2[BNames.Q3], 3) == 0.97
        # summary 2
        sum2 = baseline.ColumnSummary(data)
        res3 = sum2.binary(var4, dropna=True)
        assert res3[BNames.COUNT] == 6
        assert np.round(res3[BNames.PROP], 2) == 0.4
        assert res3[BNames.N] == 15
        assert res3[BNames.MISSING] == 0
        # summary 3
        sum3 = baseline.ColumnSummary(data)
        res4 = sum3.categorical(var8, dropna=True)
        assert res4[BNames.TABLE].shape == (4, 2)
        assert res4[BNames.TABLE][BNames.COUNT].to_list() == [6, 4, 2, 3]
        assert res4[BNames.N] == 15
        assert res4[BNames.MISSING] == 0
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_ColumnSummary_string(self):
        # getting data
        data = examples.patient_characteristics(15, 3, 3, 3, 2)
        var10 = data.columns[10]
        # summary 1
        sum1 = baseline.ColumnSummary(data)
        res1 = sum1.categorical(var10, dropna=True)
        assert res1[BNames.TABLE].shape == (4, 2)
        assert res1[BNames.TABLE][BNames.COUNT].to_list() == [6, 2, 1, 6]
        assert res1[BNames.N] == 15
        assert res1[BNames.MISSING] == 0
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_ColumnSummary_missing(self):
        # getting data, setting missing
        data = examples.patient_characteristics(15, 3, 3, 3)
        var0 = data.columns[0]
        var4 = data.columns[4]
        var8 = data.columns[8]
        data.iloc[2, : ] = np.nan
        # summary 1
        sum1 = baseline.ColumnSummary(data)
        res1 = sum1.symmetrical(var0, dropna=True)
        res2 = sum1.skewed(var0, dropna=False)
        res3 = sum1.symmetrical(var0, dropna=False)
        assert np.round(res1[BNames.MEAN], 2) == 0.39
        assert np.round(res1[BNames.SD], 2) == 0.90
        assert res1[BNames.N] == 15
        assert res1[BNames.MISSING] == 1
        assert np.isnan(res2[BNames.MEDIAN])
        assert np.isnan(res2[BNames.Q1])
        assert np.isnan(res2[BNames.Q3])
        assert np.isnan(res3[BNames.MEAN])
        # summary 2
        res3 = sum1.binary(var4,dropna=True)
        assert res3[BNames.COUNT] == 5
        assert np.round(res3[BNames.PROP], 2) == 0.36
        assert res3[BNames.N] == 15
        assert res3[BNames.MISSING] == 1
        # summary 3
        res4 = sum1.categorical(var8, dropna=True)
        assert res4[BNames.TABLE].shape == (4, 2)
        assert res4[BNames.TABLE][BNames.COUNT].to_list() == [6, 4, 2, 2]
        assert res4[BNames.N] == 15
        assert res4[BNames.MISSING] == 1
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_baseline_table(self):
        # getting data
        data = examples.patient_characteristics(15, 3, 3, 3, 2)
        # test 1
        res1 = baseline.baseline_table(data,
                                       continuous_variables=['x1', 'x3'],
                                       binary_variables=['x4', 'x5'],
                                       categorical_variables=['x10'],
                                       )
        assert res1.isna().sum().to_list() == [0, 7, 1]
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_baseline_table_nan(self):
        # getting data
        data = examples.patient_characteristics(15, 3, 3, 3, 2, prop_nan=0.15)
        # test 2
        res2 = baseline.baseline_table(data.copy(),
                                       continuous_variables=['x1', 'x3'],
                                       binary_variables=['x4', 'x5'],
                                       categorical_variables=['x10'],
                                       )
        # test 3
        res3 = baseline.baseline_table(data.copy(),
                                       continuous_variables=['x1', 'x3'],
                                       binary_variables=['x4', 'x5'],
                                       categorical_variables=['x10'],
                                       dropna=True,
                                       )
        assert res2.loc['x1'].iloc[1] == 'nan (nan; nan)'
        assert res3.loc['x1'].iloc[1] == '0.85 (0.23; 0.97)'


