from dataclasses import dataclass
from pandas.core.internals import managers
from scipy import stats
import statsmodels.api as sm

'''
Constants for the clean-data module.
'''


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Name class
class BaselineNames(object):
    '''
    Names for the `baseline` module.
    '''
    CAT_DELIM          = ': '
    OR_DELIM           = ' or '
    MEAN               = 'mean'
    SD                 = 'sd'
    N                  = 'n'
    MISSING            = 'missing'
    MEDIAN             = 'median'
    Q1                 = 'q1'
    Q3                 = 'q3'
    COUNT              = 'count'
    PROP               = 'proportion'
    TABLE              = 'table'
    TABLE_TOTAL_SAMPLE = 'Total sample size'
    TABLE_COL1         = 'Variable'
    TABLE_COL2         = 'N (%)'
    TABLE_COL3         = 'Mean (SD)'
    TABLE_COL4         = 'Median (Q1; Q3)'
    TABLE_COL5         = 'Missing (%)'
    TEST_METHOD        = 'Test'
    TEST_PVALUE        = 'P-value'
    TEST_KWARGS        = 'kwargs'
    CLS_CON_VARS       = 'continuous_variables'
    CLS_BIN_VARS       = 'binary_variables'
    CLS_CAT_VARS       = 'categorical_variables'
    CLS_ALL_VARS       = 'variables'
    CLS_STRATA         = 'strata_list'
    CLS_STRATA_NAME    = 'strata_name'
    CLS_DATA           = 'data'
    CLS_DATA_WO_MIS    = '__data'
    CLS_BASELINE       = 'table_baseline'
    CLS_BASELINE_CRUDE = '__table_baseline'
    CLS_REGRESSION     = 'table_regression_coef'
    CLS_FORMAT_WIDE    = 'wide'
    CLS_BASELINE_DIF   = 'table_baseline_tested'
    CLS_RETURN_COPY    = 'copy'
    CLS_VERBOSE        = 'verbose'
    CLS_LISTWISE       = 'listwise'
    CLS_PAIRWISE       = 'pairwise'
    CLS_MISSINGS       = 'missings'
    CLS_IGNORE         = 'ignore'
    CLS_PVAL_FORM_INQ  = 'inequality'
    CLS_PVAL_FORM_SCI  = 'scientific'
    CLS_FREQS          = 'table_frequency'
    CLS_REPLACE_NA     = 'replace_na'
    SM_ATTR_PVALUE     = 'pvalues'
    SM_ATTR_PARAMS     = 'params'
    SM_ATTR_SE         = 'bse'
    REG_ESTIMATES      = 'Point Estimates'
    REG_FORMAT_EST     = 'Formatted'
    REG_FORMAT_EST2    = 'Formatted (exp)'
    REG_SE             = 'Standard Error'
    REG_PVALUE         = 'P-values'
    REG_MODEL          = 'Model'
    REG_INT            = 'Intercept'
    REG_VAR            = 'Variable'
    REG_EXPOS          = 'Exposure'
    REG_CASES          = 'Cases'
    REG_SAMPLES        = 'Samples'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class MissingNames(object):
    '''
    Names for the `missing` module
    '''
    DATA     = '__data'
    MISSINGS = '__missings'
    ROW_N    = 'count_rows'
    COL_N    = 'count_cols'
    M_ROW_N  = 'missing_rows'
    M_COL_N  = 'missing_cols'
    M_ROW_P  = 'prop_missing_rows'
    M_COL_P  = 'prop_missing_cols'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class PruneNames(object):
    '''
    Names for the `prune` module
    '''
    FEATURE_INDEX    = 'feature_index'
    FEATURE          = 'feature'
    CORRELATION      = 'correlation'
    DATA             = 'data'
    INDEX_FOLLOW     = 'rows_with_non_positive_follow_up'
    INDEX_EVENT_TIME = 'rows_with_non_positive_event_time'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class FormatTabular(object):
    '''
    Names for the `format_tabular` module.
    '''
    phenotype_table     = 'phenotype'
    outcome_format      = 'trait'
    exposure            = 'exposure'
    exposure_format     = 'exposure_formatted'
    uniprot             = 'uniprot_id'
    uniprot_label       = 'uniprot_display_label'
    plot_label          = 'plot_label'
    pvalue              = 'pvalue'
    pvalue_log10        = 'pvalue_log10'
    file_name           = 'file_name'
    analysis            = 'analysis'
    index               = 'index'

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Tests objects
@dataclass
class BaselineTests:
    '''
    A dataclass collecting test attributes for use in the baseline module.
    
    The attributes should contain a dictionary with keys
        'Test' : A callable test/regression method,
        'P-value' : The p-value index or attribute name - should return
            a single float.
        'kwargs' : Any optional keyword argument supplied as a dict,
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __post_init__(self):
        self.__tests = {
            'KW' : {
                BaselineNames.TEST_METHOD: stats.kruskal,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {},
            },
            'AOV' : {
                BaselineNames.TEST_METHOD: stats.f_oneway,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {},
            },
            'MWU' : {
                BaselineNames.TEST_METHOD: stats.mannwhitneyu,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {},
            },
            'T' : {
                BaselineNames.TEST_METHOD: stats.ttest_ind,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {},
            },
            'CHISQ': {
                BaselineNames.TEST_METHOD: stats.chi2_contingency,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {},
            },
            'FISHER': {
                BaselineNames.TEST_METHOD: stats.fisher_exact,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {},
            },
            'OLS': {
                BaselineNames.TEST_METHOD: sm.OLS,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {},
            },
            'GLM-Binom': {
                BaselineNames.TEST_METHOD: sm.GLM,
                BaselineNames.TEST_PVALUE: 1,
                BaselineNames.TEST_KWARGS: {'family': sm.families.Binomial()},
            },
        }
    # /////////////////////////////////////////////////////////////////////////
    # Test for continuous traits with multiple strata
    @property
    def kruskal_wallis(self):
        '''
        Kruskal-Wallis test.
        '''
        return self.__tests['KW']
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def one_way_anova(self):
        '''
        One-way anova.
        '''
        return self.__tests['AOV']
    # /////////////////////////////////////////////////////////////////////////
    # Test for continuous traits with two strata
    @property
    def mann_whitney_u(self):
        '''
        Mann-Whitney-U test.
        '''
        return self.__tests['MWU']
    @property
    def t_test(self):
        '''
        Independent sample t-test.
        '''
        return self.__tests['T']
    # /////////////////////////////////////////////////////////////////////////
    # Test for 2 by 2 tables
    @property
    def fisher_exact(self):
        '''
        Fisher's exact test for two by two tables.
        '''
        return self.__tests['FISHER']
    # /////////////////////////////////////////////////////////////////////////
    # Test k by k tables
    @property
    def chi_square(self):
        '''
        Chi-square test for k by k tables.
        '''
        return self.__tests['CHISQ']
    # /////////////////////////////////////////////////////////////////////////
    # Regression models
    @property
    def ols_regression(self):
        '''
        Ordinary least square/linear regression.
        '''
        return self.__tests['OLS']
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Regression models
    @property
    def logistic_regression(self):
        '''
        Logistic regression/generalised linear model with a binomial
        distribution and a logit link function.
        '''
        return self.__tests['GLM-Binom']

